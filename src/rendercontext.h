/*
 * Copyright (C) 2022 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
 * SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERCONTEXT_H
#define RENDERCONTEXT_H

#include <private/qsgdefaultcontext_p.h>
#include <private/qsgdefaultrendercontext_p.h>

#include <QOpenGLDebugLogger>

#include "gralloctexture.h"

class RenderContext : public QSGDefaultRenderContext
{
public:
    explicit RenderContext(QSGContext* context);

    QSGTexture* createTexture(const QImage &image, uint flags = QSGRenderContext::CreateTexture_Alpha) const override;

private:
    void messageReceived(const QOpenGLDebugMessage &debugMessage);

    bool compileColorShaders() const;
    bool init() const;

    QOpenGLDebugLogger mutable m_glLogger;
    ShaderCache mutable m_cachedShaders;
};

#endif
